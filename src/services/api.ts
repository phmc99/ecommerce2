import axios from "axios";

const api = axios.create({
  baseURL: "https://blue-modas.herokuapp.com/",
});

export default api;
