import Menu from "../../components/Menu";
import ProductCard from "../../components/ProductCard";
import { useProducts } from "../../providers/productsGet";
import "./style.scss";

const Home = () => {
  const { products } = useProducts();

  return (
    <>
      <Menu />
      <div className="home-container">
        {products.map((item, index) => (
          <ProductCard
            id={item["id_produto"]}
            image={item.imagem}
            name={item.nome}
            price={item.preco}
            key={index}
          />
        ))}
      </div>
    </>
  );
};

export default Home;
