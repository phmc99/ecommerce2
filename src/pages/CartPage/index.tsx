import Menu from "../../components/Menu";
import ProductInput from "../../components/ProductInput";
import "./style.scss";
import Button from "../../components/Button";
import { useHistory } from "react-router-dom";
import { useCart } from "../../providers/cart";

const Cart = () => {
  const { cart } = useCart();
  const history = useHistory();

  const handleFinishPage = () => {
    history.push("/finish");
  };

  return (
    <div>
      <Menu inCart={true} />
      <div className="cart-page">
        {cart !== null && cart.length > 0 ? (
          <>
            <div className="cart-container">
              {cart.map((item, index) => (
                <ProductInput
                  id={item.id}
                  image={item.image}
                  name={item.name}
                  price={item.price}
                  quantity={item.quantity}
                  key={index}
                />
              ))}
            </div>

            <Button func={handleFinishPage}>Próximo</Button>
          </>
        ) : (
          <h1>Carrinho vazio</h1>
        )}
      </div>
    </div>
  );
};

export default Cart;
