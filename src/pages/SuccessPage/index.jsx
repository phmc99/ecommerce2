import { useHistory } from "react-router-dom";
import illustration from "../../assets/illustration.svg";
import Button from "../../components/Button";
import "./style.scss";
import Confetti from "react-confetti";
import { useCart } from "../../providers/cart";

const Success = () => {
  const history = useHistory();
  const { resetCart } = useCart();

  const handleClick = () => {
    resetCart();
    history.push("/");
  };

  return (
    <>
      <Confetti recycle={false} />
      <div className="success-page">
        <h1>Agradecemos a sua compra!</h1>
        <img src={illustration} alt="Ilustração" />
        <Button func={handleClick}>Voltar ao início</Button>
      </div>
    </>
  );
};

export default Success;
