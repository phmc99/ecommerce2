import toast from "react-hot-toast";
import product from "../../models/product";
import { useCart } from "../../providers/cart";
import Button from "../Button";
import "./style.scss";

interface ProductProps {
  id: string;
  image: string;
  name: string;
  price: string;
}

const ProductCard = ({ id, image, name, price }: ProductProps) => {
  const { cart, addProduct } = useCart();

  const handleClick = () => {
    const currentItem: any = cart.find((item) => item.id === id);

    if (cart.includes(currentItem)) {
      toast.error("Esse produto já foi adicionado", { duration: 1000 });
    } else {
      addProduct(product({ id, image, name, price }));
      toast.success("Produto adicionado ao carrinho", { duration: 1000 });
    }
  };

  return (
    <div className="ProductCardBox">
      <img src={image} alt={name} />
      <h2>{name}</h2>
      <span>R${price}</span>
      <Button func={handleClick}>Adicionar</Button>
    </div>
  );
};

export default ProductCard;
