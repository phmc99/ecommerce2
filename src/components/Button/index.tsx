import "./style.scss";
import { ReactNode } from "react";

interface ButtonProps {
  func: () => void;
  children: ReactNode;
}

const Button = ({ func, children }: ButtonProps) => {
  return (
    <>
      <button onClick={func}>{children}</button>
    </>
  );
};

export default Button;
