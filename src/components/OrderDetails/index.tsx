import "./style.scss";

interface OrderProps {
  quantity: number;
  name: string;
  price: string;
}

const OrderDetails = ({ quantity, name, price }: OrderProps) => {
  return (
    <li>
      <div className="info-index">
        <span>{quantity}</span>
        <span>{name}</span>
      </div>
      <span>R${price}</span>
    </li>
  );
};

export default OrderDetails;
