import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    width: "100%",
    "& label.Mui-focused": {
      color: "#1C8FC7",
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#1C8FC7",
    },
  },
});

export default useStyles;
