import TextField from "@material-ui/core/TextField";
import useStyles from "./style";
import "./style.scss";

import * as yup from "yup";
import { SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useHistory } from "react-router-dom";
import { useCart } from "../../providers/cart";

interface FormTypes {
  email: string;
  name: string;
  cell: string;
}

const Form = () => {
  const classes = useStyles();
  const { cart } = useCart();
  const history = useHistory();

  const schema = yup.object().shape({
    email: yup.string().required("E-mail obrigatório").email("E-mail inválido"),
    name: yup.string().required("Nome obrigatório"),
    cell: yup
      .string()
      .required("Telefone obrigatório")
      .matches(
        /^\(?[1-9]{2}\)? ?(?:[2-8]|9[1-9])[0-9]{3}-?[0-9]{4}$/,
        "Informe um número válido DDD+Numero"
      ),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormTypes>({
    resolver: yupResolver(schema),
  });

  const onSubmit: SubmitHandler<FormTypes> = (data) => {
    const order = {
      cart,
      customer: data,
    };

    localStorage.setItem("order", JSON.stringify(order));
    history.push("/confirm");
  };

  return (
    <div className="form-box">
      <p>
        Para finalizar a compra, informe seus dados para entrarmos em contato.
      </p>
      <form onSubmit={handleSubmit(onSubmit)}>
        <TextField
          className={classes.root}
          label="Nome"
          {...register("name")}
          error={errors.name ? true : false}
          helperText={errors.name?.message}
        />
        <TextField
          className={classes.root}
          label="E-mail"
          {...register("email")}
          error={errors.email ? true : false}
          helperText={errors.email?.message}
        />
        <TextField
          className={classes.root}
          label="Celular"
          {...register("cell")}
          error={errors.cell ? true : false}
          helperText={errors.cell?.message}
        />
        <button type="submit">Finalizar compra</button>
      </form>
    </div>
  );
};

export default Form;
