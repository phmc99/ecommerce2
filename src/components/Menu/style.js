import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  root: {
    backgroundColor: "#1c8fc7",

    "& .MuiInput-underline:after": {
      borderBottomColor: "#C0CBD1",
    },
    "& .MuiInput-underline:before": {
      borderBottomColor: "#C0CBD1",
    },
  },
});

export default useStyles;
