import { AppBar, IconButton, Toolbar } from "@material-ui/core";
import { BiArrowBack, BiCart } from "react-icons/bi";
import useStyles from "./style";
import "./style.scss";
import { useHistory } from "react-router-dom";
import { useCart } from "../../providers/cart";

interface MenuProps {
  inCart?: boolean;
}

const Menu = ({ inCart = false }: MenuProps) => {
  const classes = useStyles();
  const history = useHistory();

  const { cart } = useCart();

  const handleCartPage = () => {
    history.push("/cart");
  };

  const handlePreviousPage = () => {
    history.goBack();
  };

  return (
    <AppBar position="static" className={classes.root}>
      <Toolbar>
        {inCart ? (
          <IconButton onClick={handlePreviousPage}>
            <BiArrowBack />
          </IconButton>
        ) : (
          <>
            <IconButton onClick={handleCartPage}>
              {cart !== null && cart.length > 0 && (
                <span className="cart-products">{cart.length}</span>
              )}
              <BiCart />
            </IconButton>
          </>
        )}
      </Toolbar>
    </AppBar>
  );
};

export default Menu;
