import "./style.scss";
import { useState } from "react";
import { useCart } from "../../providers/cart";

interface ProductProps {
  id: string;
  image: string;
  name: string;
  price: string;
  quantity: number;
}

const ProductInput = ({ id, image, name, price, quantity }: ProductProps) => {
  const [value, setValue] = useState(quantity);
  const { cart, deleteProduct } = useCart();

  const handleAdd = () => {
    setValue(value + 1);
    cart.map((item) => {
      if (item.id === id) {
        item.quantity++;
      }

      return item;
    });
  };

  const handleSub = () => {
    if (value !== 1) {
      setValue(value - 1);
      cart.map((item) => {
        if (item.id === id) {
          item.quantity--;
        }

        return item;
      });
    }
  };

  const removeItem = (id: string) => {
    const product: any = cart.find((item) => item.id === id);
    deleteProduct(product);
  };

  return (
    <div className="product-content">
      <img src={image} alt={name} />
      <div className="product-info">
        <h2>{name}</h2>
        <span>R${price}</span>
        <span className="remove-product" onClick={() => removeItem(id)}>
          Remover do carrinho
        </span>
      </div>
      <div className="button-container">
        <button onClick={handleSub} disabled={value === 1}>
          -
        </button>
        <input type="number" value={value} />
        <button onClick={handleAdd}>+</button>
      </div>
    </div>
  );
};

export default ProductInput;
