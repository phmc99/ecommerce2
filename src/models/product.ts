interface ProductProps {
  id: string;
  image: string;
  name: string;
  price: string;
}

const product = ({ id, image, name, price }: ProductProps) => ({
  id,
  image,
  name,
  price,
  quantity: 1,
});

export default product;
