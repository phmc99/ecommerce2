import {
  createContext,
  ReactNode,
  useContext,
  useEffect,
  useState,
} from "react";

interface CartProduct {
  id: string;
  image: string;
  name: string;
  price: string;
  quantity: number;
}

interface CartProviderProps {
  children: ReactNode;
}

interface CartProviderData {
  cart: CartProduct[];
  addProduct: (product: CartProduct) => void;
  deleteProduct: (product: CartProduct) => void;
  resetCart: () => void;
}

const CartContext = createContext<CartProviderData>({} as CartProviderData);

export const CartProvider = ({ children }: CartProviderProps) => {
  const cartS = localStorage.getItem("cart") || "";
  const [cart, setCart] = useState<CartProduct[]>([]);

  useEffect(() => {
    if (cartS && cartS !== null) {
      setCart(JSON.parse(cartS));
    }
  }, [cartS]);

  const addProduct = (product: CartProduct) => {
    setCart([...cart, product]);
    localStorage.setItem("cart", JSON.stringify([...cart, product]));
  };

  const deleteProduct = (ProductToBeDeleted: CartProduct) => {
    const newCartList = cart.filter(
      (product: CartProduct) => product.id !== ProductToBeDeleted.id
    );
    setCart(newCartList);
    localStorage.setItem("cart", JSON.stringify(newCartList));
  };

  const resetCart = () => {
    localStorage.removeItem("cart");
    setCart([]);
  };

  return (
    <CartContext.Provider
      value={{ cart, addProduct, deleteProduct, resetCart }}
    >
      {children}
    </CartContext.Provider>
  );
};

export const useCart = () => useContext(CartContext);
