import { ReactNode } from "react";
import { CartProvider } from "./cart";
import { ProductsProvider } from "./productsGet";

interface ProvidersProps {
  children: ReactNode;
}

const Providers = ({ children }: ProvidersProps) => {
  return (
    <ProductsProvider>
      <CartProvider>{children}</CartProvider>
    </ProductsProvider>
  );
};

export default Providers;
